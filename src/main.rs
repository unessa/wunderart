#[macro_use]
extern crate gfx;
extern crate gfx_device_gl;
extern crate gfx_window_glutin;
extern crate glutin;
extern crate image;
extern crate serde;

#[macro_use(Deserialize)]
extern crate serde_derive;

use gfx::Device;
use gfx::traits::FactoryExt;
use glutin::GlContext;

mod settings;
use settings::Settings;

mod utils;

const CLEAR_COLOR: [f32; 4] = [0.6, 0.6, 0.6, 1.0];

gfx_defines! {
    vertex Vertex {
        position: [f32; 2] = "position",
        tex_coords: [f32; 2] = "tex_coords",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        texture0: gfx::TextureSampler<[f32; 4]> = "texture0",
        texture1: gfx::TextureSampler<[f32; 4]> = "texture1",
        delta_time: gfx::Global<f32> = "delta_time",
        cursor_position: gfx::Global<[f32; 2]> = "cursor_position",
        general_feeling: gfx::Global<f32> = "general_feeling",
        option0: gfx::Global<i32> = "option0",
        option1: gfx::Global<i32> = "option1",
        out: gfx::RenderTarget<utils::ColorFormat> = "out_color",
    }
}

const QUAD_VERTICES: [Vertex; 6] = [
    Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] },
    Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] },
    Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] },

    Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] },
    Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] },
];

fn main() {
    let settings = Settings::new().expect("Unable to load settings file");
    println!("{:?}", settings);

    let mut events_loop = glutin::EventsLoop::new();

    let (
        window,
        mut device,
        mut factory,
        main_color
    ) = utils::create_window(&mut events_loop, settings.application.clone());

    let mut encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();

    let vert_shader_bytes = utils::read_file_bytes(&settings.shader.vertex_shader);
    let frag_shader_bytes = utils::read_file_bytes(&settings.shader.fragment_shader);
    let texture0_bytes = utils::read_file_bytes(&settings.shader.texture0);
    let texture1_bytes = utils::read_file_bytes(&settings.shader.texture1);

    let pipeline = factory
        .create_pipeline_simple(
            &vert_shader_bytes,
            &frag_shader_bytes,
            pipe::new(),
        )
        .unwrap();

    let (
        vertex_buffer,
        slice
    ) = factory.create_vertex_buffer_with_slice(&QUAD_VERTICES, ());

    let texture0 = utils::load_texture(&mut factory, &texture0_bytes, image::PNG).unwrap();
    let texture1 = utils::load_texture(&mut factory, &texture1_bytes, image::PNG).unwrap();
    let sampler = factory.create_sampler_linear();

    let mut data = pipe::Data {
        vbuf: vertex_buffer,
        texture0: (texture0, sampler.clone()),
        texture1: (texture1, sampler),
        delta_time: 0.0,
        cursor_position: [400.0, 300.0],
        general_feeling: 1.0,
        option0: 0,
        option1: 0,
        out:  main_color,
    };

    let mut time_start = std::time::Instant::now();
    let mut running = true;
    let mut cursor_position: [f32; 2] = [0.0, 0.0];

    let resolution = if settings.application.is_retina_display {
        [(settings.application.window_width as f32) * 2.0, (settings.application.window_height as f32) * 2.0]
    } else {
        [(settings.application.window_width as f32), (settings.application.window_height as f32)]
    };

    while running {
        events_loop.poll_events(|event| {
            match event {
                glutin::Event::WindowEvent { event, .. } => match event {
                    glutin::WindowEvent::Closed => running = false,
                    glutin::WindowEvent::CursorMoved { position: (x,y), .. } => cursor_position = [x as f32, y as f32],
                    glutin::WindowEvent::MouseInput { state, .. } => {
                        if state == glutin::ElementState::Pressed {
                            data.cursor_position[0] = cursor_position[0] / resolution[0];
                            data.cursor_position[1] = 1.0 - cursor_position[1] / resolution[1];
                            println!("{:?}", data.cursor_position);
                        }
                    },
                    glutin::WindowEvent::KeyboardInput { input, .. } => {
                        match input.virtual_keycode {
                            Some(glutin::VirtualKeyCode::Up) => {
                                data.general_feeling += 0.01;
                                println!("{:?}", data.general_feeling);
                            },
                            Some(glutin::VirtualKeyCode::Down) => {
                                data.general_feeling -= 0.01;
                                println!("{:?}", data.general_feeling);
                            },
                            Some(glutin::VirtualKeyCode::Left) => {
                                data.general_feeling -= 0.1;
                                println!("{:?}", data.general_feeling);
                            },
                            Some(glutin::VirtualKeyCode::Right) => {
                                data.general_feeling += 0.1;
                                println!("{:?}", data.general_feeling);
                            },
                            Some(glutin::VirtualKeyCode::Key1) => {
                                if input.state == glutin::ElementState::Released {
                                    data.option0 = if data.option0 == 1 { 0 } else { 1 };
                                    println!("option0: {:?}", data.option0);
                                }
                            },
                            Some(glutin::VirtualKeyCode::Key2) => {
                                if input.state == glutin::ElementState::Released {
                                    data.option1 = if data.option1 == 1 { 0 } else { 1 };
                                    println!("option1: {:?}", data.option1);
                                }
                            },
                            _ => ()
                        }
                    }
                    _ => ()
                },
                _ => ()
            }
        });

        let delta = time_start.elapsed();
        time_start = std::time::Instant::now();
        let delta = delta.as_secs() as f32 + delta.subsec_nanos() as f32 / 1000_000_000.0;

        println!("{:?}", delta);

        data.delta_time += delta * 0.1;

        encoder.clear(&data.out, CLEAR_COLOR);
        encoder.draw(&slice, &pipeline, &data);
        encoder.flush(&mut device);

        window.swap_buffers().unwrap();
        device.cleanup();
    }
}
