extern crate config;

use self::config::{Config, ConfigError, File};

#[derive(Debug, Clone, Deserialize)]
pub struct ApplicationConfig {
    pub window_name: String,
    pub window_width: u32,
    pub window_height: u32,
    pub is_retina_display: bool
}

#[derive(Debug, Clone, Deserialize)]
pub struct ShaderConfig {
    pub texture0: String,
    pub texture1: String,
    pub vertex_shader: String,
    pub fragment_shader: String
}

#[derive(Debug, Clone, Deserialize)]
pub struct Settings {
    pub application: ApplicationConfig,
    pub shader: ShaderConfig
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut settings = Config::default();
        settings.merge(File::with_name("config/default"))?;
        settings.try_into()
    }
}
