use gfx::handle::RenderTargetView;
use gfx_window_glutin;
use gfx_device_gl::{Device, Factory, Resources};
use glutin::{EventsLoop, GlWindow, WindowBuilder, ContextBuilder};
use utils::{ColorFormat, DepthFormat};
use settings::ApplicationConfig;

pub fn create_window(events_loop: &mut EventsLoop, application_config: ApplicationConfig)
    -> (GlWindow, Device, Factory, RenderTargetView<Resources, ColorFormat>)
{
    let builder = WindowBuilder::new()
        .with_title(application_config.window_name)
        .with_dimensions(application_config.window_width, application_config.window_height);

    let gl_builder = ContextBuilder::new().with_vsync(true);

    let (
        window,
        device,
        factory,
        main_color,
        _main_depth
    ) = gfx_window_glutin::init::<ColorFormat, DepthFormat>(builder, gl_builder, &events_loop);

    (window, device, factory, main_color)
}
