use gfx;
use std::fs::File;
use std::io::Read;

pub type ColorFormat = gfx::format::Rgba8;
pub type DepthFormat = gfx::format::DepthStencil;

pub mod create_window;
pub use self::create_window::create_window;

pub mod load_texture;
pub use self::load_texture::load_texture;

/// Read given file name to buffer and return it.
pub fn read_file_bytes(file_name: &str) -> Vec<u8> {
    let mut file = File::open(file_name).unwrap();
    let mut buf = Vec::new();
    file.read_to_end(&mut buf).unwrap();
    buf
}
