use gfx::{texture, Factory, Resources};
use gfx::handle::{ShaderResourceView};
use image;
use std::io::Cursor;
use utils::ColorFormat;

pub fn load_texture<R, D>(factory: &mut D, data: &[u8], image_format: image::ImageFormat)
    -> Result<ShaderResourceView<R, [f32; 4]>, String>
    where R: Resources, D: Factory<R>
{
    let img = image::load(Cursor::new(data), image_format).unwrap().to_rgba();
    let (width, height) = img.dimensions();
    let kind = texture::Kind::D2(width as texture::Size, height as texture::Size, texture::AaMode::Single);
    let (_, view) = factory.create_texture_immutable_u8::<ColorFormat>(kind, texture::Mipmap::Allocated, &[&img]).unwrap();
    Ok(view)
}
