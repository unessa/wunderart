#version 330

in vec2 frag_coords;

uniform sampler2D texture0;
uniform float delta_time;

out vec4 out_color;

const float PI = 3.1415926535;

void main() {
  vec2 emitter_position;
  emitter_position.x = 0.5 + sin(delta_time * PI) * 0.25;
  emitter_position.y = 0.5 + cos(delta_time * PI) * 0.25;

  float a1 = atan(frag_coords.y - emitter_position.y, frag_coords.x - emitter_position.x);
  float r1 = sqrt(dot(frag_coords - emitter_position, frag_coords - emitter_position));
  float a2 = atan(frag_coords.y + emitter_position.y, frag_coords.x + emitter_position.x);
  float r2 = sqrt(dot(frag_coords + emitter_position, frag_coords + emitter_position));

  vec2 uv;
  uv.x = 0.1 * delta_time + (r2 - r1) * 0.25 * cos(r1 * delta_time * PI * 0.25);
  uv.y = asin(sin(a1 - 2)) / PI;

  vec3 col = texture(texture0, uv * 0.125).xyz;

  float w = exp(-10.0 * r1 * r1) + exp(-10.0 * r2 * r2);

  w += 0.25 * smoothstep(0.1, 1.0, sin(128.0 * uv.x));
  w += 0.25 * smoothstep(0.1, 1.0, cos(128.0 * uv.y));

  out_color = vec4(col + w, 1.0);
}
