#version 330

in vec2 frag_coords;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform float delta_time;
uniform vec2 cursor_position;
uniform float general_feeling;
uniform int option0;

out vec4 out_color;

const float PI = 3.1415926535;
const float FIELD_OF_VIEW = PI * 0.36;
const vec3 CAMERA_OFFSET = vec3(0.0, 0.0, 1.0);
const float SPEED_FACTOR = 8.0;
const int NUM_RAYMARCH_STEPS = 64;
const float FAR_PLANE = 75.0;
const float RAY_HITS_SURFACE_FACTOR = 0.01;
const float DISTANCE_THRESHOLD = 0.56;
const vec3 CLOUDS_FACTOR = vec3(0.25);
const vec3 LIGHT_DIRECTION = normalize(vec3(-0.2, 0.3, 0.4));

mat2 rotate_2d(float angle) {
  float c = cos(angle);
  float s = sin(angle);
  return mat2(c, s, -s, c);
}

// from https://www.shadertoy.com/view/4dS3Wd
float hash(float n) {
  return fract(sin(n) * 1e4);
}


// from https://thebookofshaders.com/10/
vec3 random_3d(vec3 position) {
  float n = sin(dot(position, vec3(12.9898, 78.233, 143.235)));
  return fract(vec3(2097152.0, 262144.0, 32768.0) * n);
}

// from https://www.shadertoy.com/view/4dS3Wd
float noise(vec3 x) {
  const vec3 step = vec3(110, 241, 171);

  vec3 i = floor(x);
  vec3 f = fract(x);

  // For performance, compute the base input to a 1D hash from the integer part of the argument and the
  // incremental change to the 1D based on the 3D -> 1D wrapping
  float n = dot(i, step);

  vec3 u = f * f * (3.0 - 2.0 * f);
  return mix(mix(mix( hash(n + dot(step, vec3(0, 0, 0))), hash(n + dot(step, vec3(1, 0, 0))), u.x),
                 mix( hash(n + dot(step, vec3(0, 1, 0))), hash(n + dot(step, vec3(1, 1, 0))), u.x), u.y),
             mix(mix( hash(n + dot(step, vec3(0, 0, 1))), hash(n + dot(step, vec3(1, 0, 1))), u.x),
                 mix( hash(n + dot(step, vec3(0, 1, 1))), hash(n + dot(step, vec3(1, 1, 1))), u.x), u.y), u.z);
}

float cloud_noise(vec3 position) {
  float res = 0.0;
  float n = noise(position * 10.0 + delta_time * 0.5);

  // first cloud noise layer
  vec3 t = sin(position.yzx * PI + cos(position.zxy * PI)) * 0.5 + 0.5;
  res += (dot(t, CLOUDS_FACTOR));

  // second cloud noise layer
  vec3 position_offset = position * 1.5 + (t - 1.5);
  t = sin(position_offset.yzx * PI + cos(position_offset.zxy * PI)) * 0.5 + .5;
  res += (dot(t, CLOUDS_FACTOR)) * 0.666;

  return res * 0.52 + n * 0.1215;
}

float smooth_max(float a, float b, float s) {
  float h = clamp(0.5 + 0.5 * (a - b) / s, 0.0, 1.0);
  return mix(b, a, h) + h * (1.0 - h) * s;
}

vec2 get_tunnel_position(in float z){
  float frequency = 0.22;
  float amplitude = 3.0;

  return vec2(
    sin(z * frequency) * amplitude,
    cos(z * frequency) * amplitude
  );
}

float distance(vec3 position) {
  float noise = cloud_noise(position);
  vec2 offset = position.xy - get_tunnel_position(position.z);
  return smooth_max(noise - 0.025, -length(offset.xy) + 0.25, 2.0);
}

vec3 raymarch(vec3 ray_origin, vec3 ray_direction) {
  vec3 rnd = random_3d(ray_direction.yzx + fract(delta_time));
  float ray_distance_travelled = dot(rnd, CLOUDS_FACTOR);
  float total_density = 0.0;
  vec3 col = vec3(0);

  for(int i = 0; i < NUM_RAYMARCH_STEPS; ++i) {
    vec3 ray_position = ray_origin + ray_direction * ray_distance_travelled;
    float closest_distance_to_surface = distance(ray_position);

    // ray hits to surface
    if(closest_distance_to_surface < ray_distance_travelled * RAY_HITS_SURFACE_FACTOR) {
      break;
    }
    // accumulated density maxes out
    if(total_density > 1.0) {
      break;
    }
    // total ray distance is beyound maximum.
    if(ray_distance_travelled > FAR_PLANE) {
      break;
    }

    float weighting_factor = mix(
      0.0,
      (1.0 - total_density) * (DISTANCE_THRESHOLD - closest_distance_to_surface),
      closest_distance_to_surface < DISTANCE_THRESHOLD
    );

    total_density += weighting_factor * weighting_factor * 8.0 + 0.015625;

    vec3 light_normal = normalize(vec3(closest_distance_to_surface - distance(ray_position)));
    float light = max(dot(LIGHT_DIRECTION, light_normal), 0.0);

    col += weighting_factor * closest_distance_to_surface * (sqrt(light) + 3.0);
    ray_distance_travelled += max(closest_distance_to_surface * 0.5, 0.05);
  }

  return max(col, 0.0);
}

vec3 get_ray_direction(vec2 position, vec3 ray_origin, vec3 look_at) {
  vec3 forward = normalize(look_at - ray_origin);
  vec3 right = normalize(vec3(forward.z, 0.0, -forward.x));
  vec3 up = cross(forward, right);

  vec3 ray_direction = normalize(
    forward +
    FIELD_OF_VIEW * position.x * right +
    FIELD_OF_VIEW * position.y * up);

  vec2 camera_path = get_tunnel_position(look_at.z);
  ray_direction.xy *= rotate_2d(-camera_path.x * 0.0416);
  ray_direction.yz *= rotate_2d(-camera_path.y * 0.0625);

  return ray_direction;
}

void main() {
  vec2 uv = frag_coords - 0.5;
  vec3 ray_origin = vec3(0, 0, delta_time * SPEED_FACTOR);
  vec3 look_at = ray_origin + CAMERA_OFFSET;

  ray_origin.xy += get_tunnel_position(ray_origin.z);
  look_at.xy += get_tunnel_position(look_at.z);

  vec3 ray_direction = get_ray_direction(uv, ray_origin, look_at);

  vec3 col = raymarch(ray_origin, ray_direction);

  // Colorize
  col = mix(
    pow(vec3(1.3, 0.75, 0.55) * col, vec3(1.0, 2.0, 10.0)),
    col,
    dot(cos(ray_direction * 6.0 + sin(ray_direction.yzx * 6.0)), CLOUDS_FACTOR) * 0.2 + 0.8);

  // Contrast
  col = pow(col, vec3(general_feeling));

  // Smoothing
  col = sqrt(min(col, 1.0));

  out_color = vec4(col, 1.0);
}
