#version 330

in vec2 frag_coords;

uniform sampler2D texture0;
uniform float delta_time;
uniform vec2 cursor_position;
uniform float general_feeling; // Change effect lightness / darkness

out vec4 out_color;

const vec3 COLOR_BACKGROUND = vec3(1.0, 0.98, 0.94);
const vec3 COLOR_FOREGROUND = vec3(0.01, 0.01, 0.01);
const vec2 OFFSET_POSITION = vec2(0.5, 0.95);
const float SPEED_FACTOR = 0.5;
const int NUM_OCTAVES = 8;
const float AMPLITUDE = 0.231;
const float FREQUENCY = 4.0;

/**
 * Based on Morgan McGuire @morgan3d
 * https://www.shadertoy.com/view/4dS3Wd
 *
 * @param position The pixel position
 */
float random (in vec2 position) {
  return fract(sin(dot(position.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}

/**
 * Based on Morgan McGuire @morgan3d
 * https://www.shadertoy.com/view/4dS3Wd
 *
 * @param position The pixel position
 */
float noise(in vec2 position) {
  vec2 i = floor(position);
  vec2 f = fract(position);

  // Four corners in 2D of a tile
  float a = random(i);
  float b = random(i + vec2(1.0, 0.0));
  float c = random(i + vec2(0.0, 1.0));
  float d = random(i + vec2(1.0, 1.0));

  vec2 u = f * f * (3.0 - 2.0 * f);

  return mix(a, b, u.x) +
    (c - a)* u.y * (1.0 - u.x) +
    (d - b) * u.x * u.y;
}

/**
 * fractal Brownian Motion
 *
 * @param position The pixel position
 * @param num_octaves The number of octaves (iterations)
 * @param AM Amplitude modulated (gain)
 * @param FM Frequency modulated (lacunarity)
 */
float fbm(in vec2 position, int num_octaves, float AM, float FM) {
  float sum = 0.0;
  float amplitude = 1.0;

  for(int i = 0; i < num_octaves; ++i) {
    sum += amplitude * noise(position);
    amplitude *= AM;
    position *= FM;
  }

  return sum;
}

void main() {
  float speed = delta_time * SPEED_FACTOR;
  vec2 uv = OFFSET_POSITION + frag_coords;

  vec2 q = vec2(0.0);
  q.x = fbm(uv + speed, NUM_OCTAVES, AMPLITUDE, FREQUENCY);
  q.y = fbm(uv - speed, NUM_OCTAVES, AMPLITUDE, FREQUENCY);

  vec2 r = vec2(0.0);
  r.x = fbm(uv + q + speed, NUM_OCTAVES, AMPLITUDE, FREQUENCY);
  r.y = fbm(uv + q - speed, NUM_OCTAVES, AMPLITUDE, FREQUENCY);

  float f = fbm(uv + r, NUM_OCTAVES, AMPLITUDE, FREQUENCY);

  vec3 color = mix(
    COLOR_FOREGROUND,
    COLOR_BACKGROUND,
    clamp((f * f) * 8.0 * general_feeling, 0.0, 1.0)
  );

  vec3 final_color= f * f * color;
  out_color = vec4(final_color, 1.0);
}
