#version 330

in vec2 frag_coords;

uniform sampler2D texture0;
uniform float delta_time;
uniform vec2 cursor_position; // Used to change current offset position

out vec4 out_color;

const float PI = 3.1415926535;
const float SPEED_FACTOR = 2.0;
const vec3 COLOR_BACKGROUND = vec3(0.01, 0.025, 0.1);

void main() {
  float speed = delta_time * SPEED_FACTOR;
  float r = sqrt(dot(frag_coords - cursor_position, frag_coords - cursor_position));
  vec2 uv = -1.0 + 2.0 * frag_coords * sin(r * PI);
  vec3 color = COLOR_BACKGROUND;

  for(float i = 0.0; i < 16.0; i++) {
    float wave_width = abs(1.0 / (200.0 * uv.y));
    uv.y += (0.08 * sin(uv.x + i / 32.0 + speed));
    color += vec3(wave_width * sin(r * PI), wave_width, wave_width * 2.0);
  }

  out_color = vec4(color, 1.0);
}
