#version 330

in vec2 frag_coords;

uniform sampler2D texture0;
uniform float delta_time;
uniform vec2 cursor_position;
uniform float general_feeling;

out vec4 out_color;

const vec2 OFFSET_POSITION = vec2(0.5);
const float RADIUES = 0.4;
const float LINE_WIDTH = 0.05;
const vec3 COLOR_BACKGROUND = vec3(0.9, 0.88, 0.84);

float variation(vec2 v1, vec2 v2, float strength, float speed) {
  vec2 nv1 = normalize(v1);
  vec2 nv2 = normalize(v2);
  float d = dot(nv1, nv2);
  return sin(d * strength + delta_time * speed) * 0.1 * general_feeling;
}

vec3 draw_circle(vec2 position, vec2 center, float radius, float width) {
  vec2 diff = center - position;
  float len = length(diff);
  len += variation(diff, vec2(0.0, 1.0), 2.0, 16.0);
  len -= variation(diff, vec2(1.0, 0.0), 4.0, 16.0);
  float circle = smoothstep(radius - width, radius, len) - smoothstep(radius, radius + width, len);
  return vec3(circle);
}

void main() {
  vec3 color = COLOR_BACKGROUND;

  // background
  vec2 v = frag_coords * general_feeling;
  color *= vec3(0.5 + v.x, v.y, 0.5 - v.x * v.y);

  // circle
  color += draw_circle(frag_coords, OFFSET_POSITION, RADIUES, LINE_WIDTH);

	out_color = vec4(color, 1.0);
}
