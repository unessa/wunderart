#version 330

in vec2 frag_coords;

uniform sampler2D texture0;
uniform float delta_time;
uniform vec2 cursor_position;
uniform float general_feeling;
uniform int option0; // show/hide lines
uniform int option1; // show/hide dots

out vec4 out_color;

const float PI = 3.1415926535;
const float PI2 = 6.2831853072;
const vec3 COLOR_BACKGROUND = vec3(0.176, 0.174, 0.17);
const vec3 COLOR_EFFECT = vec3(0.3, 0.7, 0.5);
const float NUM_SEGMENTS = 8.0;
const vec2 OFFSET_POSITION = vec2(0.5);

float render_lines(float u, float v, float index) {
  return mix(
    1.0,
    smoothstep(-0.95, -0.94, sin(8.0 * PI2 * v + 3.0 * u + 2.0 * index)),
    smoothstep(0.4, 0.5, sin(index * 13.0))
  );
}

float render_dots(float u, float v, float index) {
  return mix(
    1.0,
    smoothstep(-0.8, -0.7, sin(80.0 * v) * sin(20.0 * u)),
    smoothstep(0.4, 0.5, sin(index * 17.0))
  );
}

float render_border(float u) {
  return smoothstep(0.01, 0.015, 0.5 - abs(u - 0.5));
}

vec3 render_highlights(float w) {
  return vec3(0.0, 0.1, 0.3) + w * vec3(1.1, 0.7, 0.4);
}

float render_shadow(float u, float w) {
  return mix(1.0 - u, 1.0, w * w * 0.01);
}

vec4 render_segment(float x0, float x1, vec2 uv, float id, float speed, float index) {
  float u = (uv.x - x0) / (x1 - x0);
  float v = (id + 0.5) * speed + 2.0 * uv.y / PI + index * 2.0;
  float w = (x1 - x0);

  vec3 color = COLOR_EFFECT;

  // gradient
  color += 0.5 * sin(index + id);

  // lines
  if (option0 == 1) {
    color *= render_lines(u, v, index);
  }

  // dots
  if (option1 == 1) {
    color *= render_dots(u, v, index);
  }

  // black border
  color *= render_border(u);

  // lighting
  color *= render_highlights(w);
  color *= render_shadow(u, w);

  float edge = 1.0 - smoothstep(0.5, 0.5 + 0.02 / w, abs(u - 0.5));

  return vec4(color, edge * step(x0, x1));
}

void main() {
  vec2 uv = (OFFSET_POSITION - frag_coords) * NUM_SEGMENTS;

  // twist
//  uv = vec2(length(uv), atan(uv.y, uv.x));

  float id = floor(uv.x * 0.5);
  float speed = delta_time * 2.0;
  vec2 uvr = vec2(mod(uv.x, 2.0) - 1.0, uv.y);

  // twist
  float a = uvr.y + (id + 0.5) * speed;
  // circle
//  float a = id * speed;

  const float radius = 0.95;
  float x0 = radius * sin(a);

  vec3 color = COLOR_BACKGROUND;

  for(float i = 0.2; i <= 1.2; i += 0.2) {
    float x1 = radius * sin(a + PI2 * i);
    vec4 seg = render_segment(x0, x1, uvr, id, speed, i);
    color = mix(color, seg.rgb, seg.a);
    x0 = x1;
  }

 	out_color = vec4(color, 1.0);
}
